"""Managing Jenkins agents"""

import os
import typing

import genshi.template
import lxml

import api.crawler
import commandline
import connections
import tables
import projects


def complete_agent(**kwargs):
    """completer for agent arguments"""
    parsed_args = kwargs["parsed_args"]
    with connections.NoninteractiveConnections(
        parsed_args.credentials, subdomain=parsed_args.project
    ) as connection:
        agent_list = connection.jenkins.get_nodes_full()
        return [agent["displayName"] for agent in agent_list]


Name = typing.NewType("Name", str)
Name.__doc__ = "agent name"
commandline.set_completer(Name, complete_agent)

Description = typing.NewType("Description", str)
Description.__doc__ = "agent description"

Directory = typing.NewType("Directory", str)
Directory.__doc__ = "remote root directory"

Labels = typing.NewType("Labels", str)
Labels.__doc__ = "labels"

Message = typing.NewType("Message", str)
Message.__doc__ = "offline message"

LIST_SPEC: api.spec.DictSpec = {
    "display_name": "displayName",
    "description": "description",
    "idle": ("idle", api.crawler.yes_no_of_bool),
    "offline": "offline",
    "offline_cause": "offlineCauseReason",
}

ListFilter = typing.NewType("ListFilter", str)
tables.init_list_filter_type(ListFilter, LIST_SPEC)

Progressive = typing.NewType("Progressive", bool)
Progressive.__doc__ = "keep showing progress until the agent is disconnected"

Executors = typing.NewType("Executors", int)
Executors.__doc__ = "number of executors"

Command = typing.NewType("Command", str)
Command.__doc__ = "command to launch on the master"


@commandline.RegisterCategory
class Agent(commandline.Category):
    """managing Jenkins agents"""

    @commandline.RegisterAction
    def list(
        self, project: projects.Name, list_filter: typing.Iterable[ListFilter]
    ) -> None:
        """list Jenkins agents"""
        with connections.Connections(
            self.config, subdomain=project
        ) as connection:
            response = connection.retry_loop(connection.jenkins.get_nodes_full)
            rows = api.spec.remap_dict_list(response, LIST_SPEC)
        tables.filter_and_show_list(rows, list_filter, self.config.compact)

    @commandline.RegisterAction
    def launch(self, project: projects.Name, agent: Name) -> None:
        """launch Jenkins agent on node"""
        with connections.Connections(
            self.config, subdomain=project
        ) as connection:
            connection.jenkins.launch_slave_agent(agent)

    @commandline.RegisterAction
    def delete(self, project: projects.Name, agent: Name) -> None:
        """delete agent"""
        with connections.Connections(
            self.config, subdomain=project
        ) as connection:
            connection.jenkins.delete_node(agent)

    @commandline.RegisterAction
    def disconnect(
        self,
        project: projects.Name,
        agent: Name,
        message: typing.Optional[Message] = None,
    ) -> None:
        """disconnect agent from virtual machine"""
        with connections.Connections(
            self.config, subdomain=project
        ) as connection:
            connection.jenkins.disconnect_node(agent, offline_message=message)

    # Front-end commands can have many parameters...
    # pylint: disable=too-many-arguments
    @commandline.RegisterAction
    def set(
        self,
        project: projects.Name,
        agent: Name,
        description: typing.Optional[Description] = None,
        directory: typing.Optional[Directory] = None,
        executors: typing.Optional[Executors] = None,
        labels: typing.Optional[Labels] = None,
        command: typing.Optional[Command] = None,
    ) -> None:
        """change Jenkins agent configuration"""
        with connections.Connections(
            self.config, subdomain=project
        ) as connection:
            config_str = connection.jenkins.get_node_config_raw(agent)
            config = lxml.etree.XML(config_str.content)
            if description is None and directory is None and executors is None:
                print(lxml.etree.tostring(config).decode("utf-8"))
                return
            if description is not None:
                config.find("description").text = description
            if directory is not None:
                config.find("directory").text = directory
            if executors is not None:
                config.find("numExecutors").text = str(executors)
            if labels is not None:
                config.find("label").text = str(labels)
            if command is not None:
                loader = new_template_loader()
                template = loader.load("command-launcher.xml")
                launcher_str = template.generate(agent_command=command).render()
                launcher = lxml.etree.XML(launcher_str)
                config.find("launcher").replaceWith(launcher.launcher)
            print(lxml.etree.tostring(config))
            connection.jenkins.reconfig_node_raw(
                agent, lxml.etree.tostring(config)
            )

    @commandline.RegisterAction
    def log(
        self,
        project: projects.Name,
        agent: Name,
        progressive: Progressive = Progressive(False),
    ) -> None:
        """show Jenkins agent connection log"""
        with connections.Connections(
            self.config, subdomain=project
        ) as connection:
            connection.jenkins.show_computer_log(agent, progressive=progressive)


def new_template_loader():
    """return an instance of Genshi template loader initialized with the
    templates/ directory"""
    return genshi.template.loader.TemplateLoader(
        os.path.join(os.path.dirname(__file__), "templates")
    )
