"""CloudStack commands"""

import typing

import commandline
import connections
import users


@commandline.RegisterCategory
class Cloudstack(commandline.Category):
    """CloudStack commands"""

    @commandline.RegisterAction
    def generate_keys(self, name: typing.Optional[users.Name] = None) -> None:
        """generate CloudStack API keys"""
        with connections.Connections(self.config) as connection:
            email = connection.portal.users[name].email
            uid = connection.cloudstack.listUsers(username=email)["user"][0][
                "id"
            ]
            keys = connection.cloudstack.registerUserKeys(id=uid)["userkeys"]
            dump_keys(connection, keys)

    @commandline.RegisterAction
    def get_keys(self, name: typing.Optional[users.Name] = None) -> None:
        """get CloudStack API keys"""
        with connections.Connections(self.config) as connection:
            email = connection.portal.users[name].email
            uid = connection.cloudstack.listUsers(username=email)["user"][0][
                "id"
            ]
            keys = connection.cloudstack.getUserKeys(id=uid)["userkeys"]
            dump_keys(connection, keys)


def dump_keys(connection, keys):
    """dump CloudStack keys"""
    print("[cloudstack]")
    print(f"endpoint = {connection.cloudstack_api_url}")
    print(f"key = {keys['apikey']}")
    print(f"secret = {keys['secretkey']}")
