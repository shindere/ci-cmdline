"""base class for classes indexed by a key"""

import functools


@functools.total_ordering
class KeyIndexed:
    """base class for classes indexed by a key"""

    def _key(self):
        """return key"""

    def __hash__(self) -> int:
        return hash(self._key())

    def __lt__(self, other) -> bool:
        return isinstance(other, KeyIndexed) and self._key() < other._key()

    def __eq__(self, other) -> bool:
        return isinstance(other, KeyIndexed) and self._key() == other._key()
