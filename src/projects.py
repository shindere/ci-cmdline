"""Managing projects"""

import textwrap
import typing

import api.crawler
import api.portal
import commandline
import connections
import tables
import users


def complete_project(**kwargs):
    """complete project arguments"""
    with connections.NoninteractiveConnections(
        kwargs["parsed_args"].credentials
    ) as connection:
        username = connection.username
        if username:
            return [
                project.short_name
                for project in connection.portal.users[username].get_projects()
            ]
    return None


Name = typing.NewType("Name", str)
Name.__doc__ = "project name"
commandline.set_completer(Name, complete_project)

ProjectListFilter = typing.NewType("ProjectListFilter", str)
tables.init_list_filter_type(
    ProjectListFilter,
    api.crawler.PROJECT_LIST_SPEC_REGULAR_USER,
    api.crawler.PROJECT_LIST_SPEC_ADMIN,
)

LogFilter = typing.NewType("LogFilter", str)
tables.init_list_filter_type(LogFilter, api.crawler.PROJECT_LOG_LIST_SPEC)

Page = typing.NewType("Page", int)
Page.__doc__ = "index of the page to display"

Limit = typing.NewType("Limit", int)
Limit.__doc__ = "entry count by page"

MemoryLimit = typing.NewType("MemoryLimit", int)
MemoryLimit.__doc__ = "\
    Memory (RAM) limit for Jenkins instance (in Go) (for CI admin only)"


def complete_server(**kwargs):
    """complete server arguments"""
    parsed_args = kwargs["parsed_args"]
    if parsed_args.qlfci:
        return [f"qlf-ci-jenkins{i}.inria.fr" for i in (1, 2)]
    return [f"ci-jenkins{i}.inria.fr" for i in range(1, 17)]


Server = typing.NewType("Server", str)
Server.__doc__ = "Server for Jenkins instance"
commandline.set_completer(Server, complete_server)

SHORT_NAME_SPEC: api.spec.TextSpec = {
    """must be between 2 and 27 characters long""": lambda s: 2 <= len(s) <= 27,
    """must start with a lowercase letter""": lambda s: "a" <= s[0] <= "z",
    """can contain ASCII letters 'a' through 'z', the digits '0' through '9',
    and the hyphen ('-')""": lambda s: all(
        "a" <= c <= "z" or "0" <= c <= "9" or c == "-" for c in s
    ),
}

FULL_NAME_SPEC: api.spec.TextSpec = {
    """should not be empty""": lambda s: len(s) >= 1
}

DESCRIPTION_SPEC: api.spec.TextSpec = {
    """should not be empty""": lambda s: len(s) >= 1
}

Jenkins = typing.NewType("Jenkins", bool)
Jenkins.__doc__ = "enable or disable Jenkins"

NoJenkins = typing.NewType("NoJenkins", bool)
NoJenkins.__doc__ = "disable Jenkins"


@commandline.RegisterCategory
class Project(commandline.Category):
    """managing projects"""

    ShortName = typing.NewType("ShortName", str)
    ShortName.__doc__ = f"""
        project short name

        {api.spec.get_spec_text('SHORT-NAME', SHORT_NAME_SPEC)}
        """

    FullName = typing.NewType("FullName", str)
    FullName.__doc__ = f"""
        project long name

        {api.spec.get_spec_text('FULL-NAME', FULL_NAME_SPEC)}
        """

    Description = typing.NewType("Description", str)
    Description.__doc__ = f"""\
        project description, and tools used to build and test the software

        {api.spec.get_spec_text('DESCRIPTION', DESCRIPTION_SPEC)}
        """

    Public = typing.NewType("Public", bool)
    Public.__doc__ = "project visible in the project list"

    # Front-end commands can have many parameters...
    # pylint: disable=too-many-arguments
    @commandline.RegisterAction
    def create(
        self,
        short_name: ShortName,
        full_name: FullName,
        description: Description,
        public: Public = Public(False),
        no_jenkins: NoJenkins = NoJenkins(False),
    ):
        """create a new project"""
        api.spec.check_text_spec(short_name, "short name", SHORT_NAME_SPEC)
        api.spec.check_text_spec(full_name, "full name", FULL_NAME_SPEC)
        api.spec.check_text_spec(description, "description", DESCRIPTION_SPEC)
        with connections.Connections(self.config) as connection:
            connection.portal.projects.create(
                short_name,
                full_name,
                public,
                description,
                jenkins=not (no_jenkins),
            )

    @commandline.RegisterAction
    def list(
        self,
        list_filter: typing.Iterable[ProjectListFilter],
        user: typing.Optional[users.Name] = None,
    ) -> None:
        """list projects"""
        filters = tables.Filters.parse(list_filter)
        with connections.Connections(self.config) as connection:
            connection.portal.login()
            if user is None:
                try:
                    name: typing.Optional[str] = next(
                        name
                        for name in (
                            condition.get_simple_equality()
                            for condition in filters.conditions
                            if condition.key == "short_name"
                        )
                        if name is not None
                    )
                except StopIteration:
                    name = None
                if name is None:
                    rows = connection.portal.projects
                else:
                    # Optimization if there is a condition of the form
                    # short_name=PROJECT-NAME
                    rows = connection.portal.projects.search(name)
            else:
                rows = connection.portal.users[user].get_projects()
        filters.show_list(tables.listable_to_table(rows), self.config.compact)

    ReallyDelete = typing.NewType("ReallyDelete", bool)
    ReallyDelete.__doc__ = "purge the project (needs administration access)"

    @commandline.RegisterAction
    def delete(
        self, project: Name, really: ReallyDelete = ReallyDelete(False)
    ) -> None:
        """delete a project"""
        with connections.Connections(self.config) as connection:
            if really:
                connection.portal.projects[project].really_delete()
            else:
                connection.portal.projects[project].delete()

    @commandline.RegisterAction
    def join(self, project: Name) -> None:
        """join a project"""
        with connections.Connections(self.config) as connection:
            connection.portal.projects[project].join()

    @commandline.RegisterAction
    def suspend(self, project: Name) -> None:
        """suspend a project (CI admin only)"""
        with connections.Connections(self.config) as connection:
            connection.portal.projects[project].suspend()

    @commandline.RegisterAction
    def resume(self, project: Name) -> None:
        """resume a project (CI admin only)"""
        with connections.Connections(self.config) as connection:
            connection.portal.projects[project].resume()

    # Front-end commands can have many parameters...
    # pylint: disable=too-many-arguments
    @commandline.RegisterAction
    def set(
        self,
        project: Name,
        name: typing.Optional[FullName] = None,
        public: typing.Optional[Public] = None,
        description: typing.Optional[Description] = None,
        jenkins: typing.Optional[Jenkins] = None,
        memory_limit: typing.Optional[MemoryLimit] = None,
        server: typing.Optional[Server] = None,
    ):
        """edit project properties

        If no modification is given, print the current project properties."""
        if name is not None:
            api.spec.check_text_spec(name, "full name", FULL_NAME_SPEC)
        if description is not None:
            api.spec.check_text_spec(
                description, "description", DESCRIPTION_SPEC
            )
        with connections.Connections(self.config) as connection:
            config = connection.portal.projects[project].fetch_config()
            modified = False
            if name is not None:
                config.full_name = name
                modified = True
            if public is not None:
                config.public = public
                modified = True
            if description is not None:
                config.description = description
                modified = True
            if jenkins is not None:
                config.jenkins = jenkins
                modified = True
            if memory_limit is not None:
                config.memory_limit = memory_limit
                modified = True
            if server is not None:
                config.server = server
                modified = True
            if modified:
                config.set()
            else:
                print(
                    f"""\
full name: {config.full_name}
public: {config.public}
description: {config.description}\
"""
                )
                if config.jenkins:
                    print(
                        textwrap.dedent(
                            f"""\
                            CI software: Jenkins
                            memory_limit: {config.memory_limit}
                            server: {config.server}
                            available servers:\
                            """
                        ),
                        end="",
                    )
                    available_servers = "".join(
                        {
                            "\n - " + server
                            for server in config.available_servers
                        }
                    )
                    print(available_servers)
                else:
                    print("CI software: none")

    @commandline.RegisterAction
    def log(
        self,
        project: Name,
        log_filter: typing.Iterable[LogFilter],
        page: Page = Page(1),
        limit: Limit = Limit(20),
    ) -> None:
        """show project log"""
        with connections.Connections(self.config) as connection:
            tables.filter_and_show_list(
                connection.portal.projects[project].log(page, limit),
                log_filter,
                self.config.compact,
            )

    @commandline.RegisterAction
    def download_key(
        self,
        project: Name,
        output: typing.Optional[commandline.OutputFilename] = None,
    ) -> None:
        """download public SSH key for project virtual machines

        By default, the key is saved to the file SHORT-NAME.pub where
        SHORT-NAME is the name of the project. If '-' is given as output
        file name, the key is printed to standard output."""
        if output is None:
            output_file = project + ".pub"
        else:
            output_file = output
        with connections.Connections(self.config) as connection:
            key = connection.portal.projects[project].download_key()
            if output_file == "-":
                print(key)
            else:
                with open(output_file, "w") as file:
                    file.write(key)
