"""Managing resource limits"""

import enum
import sys
import typing

import api.crawler
import commandline
import connections
import projects
import tables

# See https://github.com/python/typeshed/issues/3500#issuecomment-560958608
if sys.version_info >= (3, 8):
    from typing import TypedDict  # pylint: disable=no-name-in-module
else:
    from typing_extensions import TypedDict


class Resource(enum.Enum):
    """Resource IDs"""

    CPU = 8
    Memory = 9
    PrimaryStorage = 10
    SecondaryStorage = 11


class Limits(TypedDict):
    """Limits for a given resource"""

    typename: str
    max: int


LIST_SPEC: api.spec.DictSpec = {
    "#": ("resourcetype", int),
    "typename": "resourcetypename",
    "max": ("max", int),
}


def get_dict(
    connection: connections.Connections, domainid: str
) -> typing.Mapping[int, typing.Dict[str, typing.Any]]:
    """enumerate resource limits"""
    result = {}
    resourcelimits = connection.cloudstack.listResourceLimits(
        fetch_list=True, domainid=domainid
    )
    for resourcelimit in resourcelimits:
        result[int(resourcelimit["resourcetype"])] = {
            "typename": resourcelimit["resourcetypename"],
            "max": int(resourcelimit["max"]),
        }
    return result


def update_resource_limit(
    connection: connections.Connections,
    domainid: str,
    resource: Resource,
    value: int,
) -> None:
    """change the value of a resource limit"""
    response = connection.cloudstack.updateResourceLimit(
        domainid=domainid, resourceType=resource.value, max=str(value)
    )
    if response["resourcelimit"]["max"] != value:
        raise commandline.Failure(f"Server error: {response}")


ListFilter = typing.NewType("ListFilter", str)
tables.init_list_filter_type(ListFilter, LIST_SPEC)


@commandline.RegisterCategory
class ResourceLimit(commandline.Category):
    """managing resource limits"""

    @commandline.RegisterAction
    def list(
        self, project: projects.Name, list_filter: typing.Iterable[ListFilter]
    ) -> None:
        """list resource limits"""
        with connections.Connections(self.config) as connection:
            domainid = connection.get_project_domainid(project)
            rows = tables.rows_of_dict(get_dict(connection, domainid))
        tables.filter_and_show_list(rows, list_filter, self.config.compact)

    Memory = typing.NewType("Memory", str)
    Memory.__doc__ = "new limit for memory (in MB)"

    CPU = typing.NewType("CPU", str)
    CPU.__doc__ = "new limit on number of cores"

    PrimaryStorage = typing.NewType("PrimaryStorage", str)
    PrimaryStorage.__doc__ = "new limit for primary storage (in GB)"

    SecondaryStorage = typing.NewType("SecondaryStorage", str)
    SecondaryStorage.__doc__ = "new limit for secondary storage (in GB)"

    # Front-end commands can have many parameters...
    # pylint: disable=too-many-arguments
    @commandline.RegisterAction
    def update(
        self,
        project: projects.Name,
        memory: typing.Optional[Memory] = None,
        cpu: typing.Optional[CPU] = None,
        primary_storage: typing.Optional[PrimaryStorage] = None,
        secondary_storage: typing.Optional[SecondaryStorage] = None,
    ) -> None:
        """change resource limits"""
        with connections.Connections(self.config) as connection:
            domainid = connection.get_project_domainid(project)
            limits_cache = None

            def limits():
                nonlocal limits_cache
                if limits_cache is None:
                    limits_cache = get_dict(connection, domainid)
                return limits_cache

            def update_resource(
                resource: Resource, parameter: typing.Optional[str]
            ) -> None:
                if parameter is None:
                    return
                relative = commandline.str_to_relative(parameter)
                value = relative(lambda: limits()[resource.value]["max"])
                update_resource_limit(connection, domainid, resource, value)

            update_resource(Resource.Memory, memory)
            update_resource(Resource.CPU, cpu)
            update_resource(Resource.PrimaryStorage, primary_storage)
            update_resource(Resource.SecondaryStorage, secondary_storage)
