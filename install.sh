#!/bin/bash
set -e
files=(
    ci
    api/connection.py
    api/cloudstack.py
    api/crawler.py
    api/key_indexed.py
    api/jenkins_ext.py
    api/lazy.py
    api/portal.py
    api/spec.py
    templates/pipeline-job.xml
    commandline.py
    connections.py
    deleted_projects.py
    jenkins_instances.py
    jenkins_queue.py
    members.py
    agents.py
    builds.py
    jobs.py
    plugins.py
    projects.py
    resource_limits.py
    vms.py
    ssh_keys.py
    tables.py
    tokens.py
    templates.py
    users.py
    volumes.py
    isos.py
    cloudstack_commands.py
)

default_target="$HOME/.local/lib/ci-cmdline/"
yes=
verbose=
inplace=
target=
pip=true
bin=true
completion=true
bin_installed=
function display_help() {
    cat <<EOF
./install.sh [--yes] [--in-place | --target PATH] [--disable-pip] \\
    [--disable-bin] [--disable-completion]

Install ci.inria.fr command-line. By default, the script asks confirmation for
each step.
The script installs dependencies via pip, copies source files to
$default_target (can be changed with '--in-place' or '--target PATH' options),
adds a symbolic link in ~/bin/ci and installs shell autocompletion.

        -h, --help              display this help and exit
        --yes                   use default for all questions
        --verbose               print executed commands
        --in-place              use files in place (do not copy)
        --target PATH           copy files to PATH [default: $default_target]
        --disable-pip           do not install dependencies via pip
        --disable-bin           do not link ci in ~/bin
        --disable-completion    do not update ~/.profile for autocompletion
EOF
}
while [ "$#" -gt 0 ]; do
    argument="$1"
    shift
    case "$argument" in
    -h | --help)
        display_help
        exit 0
        ;;
    --yes)
        yes=true
        ;;
    --verbose)
        verbose=true
        ;;
    --in-place)
        inplace=true
        ;;
    --target)
        target="$1"
        shift
        ;;
    --disable-pip)
        pip=
        ;;
    --disable-bin)
        bin=
        ;;
    --disable-completion)
        completion=
        ;;
    *)
        cat >/dev/fd/1 <<EOF
Unknown argument: $argument.
Execute ./install.sh --help for usage.
EOF
        exit 1
        ;;
    esac
done
if [ -n "$inplace" ] && [ -n "$target" ]; then
    cat >/dev/fd/2 <<EOF
Options --in-place and --target are mutually exclusive.
Execute ./install.sh --help for usage.
EOF
    exit 1
fi

declare sure

function ask() {
    local -n variable="$1"
    prompt="$2"
    default="$3"
    if [ -z "$yes" ]; then
        echo -n "$prompt [$default] "
        read -r variable
        if [ -z "$variable" ]; then
            variable="$default"
        fi
    else
        variable="$default"
    fi
}

function log() {
    if [ -n "$verbose" ]; then
        echo "$@"
    fi
    "$@"
}

if [ -n "$pip" ]; then
    ask sure "Install dependencies via pip?" y
    if [ "$sure" == y ]; then
        if ! command -v pip3 >/dev/null; then
            install_pip=(sudo apt-get install --yes python3-pip)
            ask sure "pip3 does not look to be installed.
Install it with '${install_pip[*]}'?" y
            if [ "$sure" == y ]; then
                log "${install_pip[@]}"
            fi
        fi
        # Extract "pip3 install" command from README.md:
        # search for "pip3 install" pattern and then concat lines
        # while they are ending with "\".
        eval "$(sed -n '/^pip3 install/{:x;/\\$/{N;bx};p}' README.md)"
    fi
fi

if [ -z "$inplace" ]; then
    if [ -z "$target" ]; then
        ask target "
(Run './install.sh --in-place' to use ci without copying files.)
Directory to install files?" "$default_target"
    fi
    if [ -e "$target" ]; then
        ask sure "'$target' already exists. Continue?" y
        if [ "$sure" != "y" ]; then
            exit 1
        fi
    fi
    log mkdir -p "$target/api"
    for file in "${files[@]}"; do
        dirname="$(dirname "$file")"
        log cp "src/$file" "$target$dirname"
    done
else
    target="$PWD/src/"
fi

if [ -n "$bin" ]; then
    ci="$target"ci
    if [ ! -d "$HOME/.local/bin" ] && [ -d "$HOME/bin" ]; then
        bindir="$HOME/bin"
    else
        bindir="$HOME/.local/bin"
    fi
    link_target="$bindir/ci"
    if [ -e "$link_target" ] || [ -L "$link_target" ]; then
        ask sure "Replace '$link_target' by a link to '$ci'?" y
        if [ "$sure" == y ]; then
            log ln -sf "$ci" "$link_target"
            bin_installed=true
        fi
    else
        ask sure "Link '$ci' from '$link_target'?" y
        if [ "$sure" == y ]; then
            if [ ! -d "$bindir" ]; then
                ask sure "'$bindir' does not exist. Create it?" y
                if [ "$sure" == y ]; then
                    log mkdir -p "$bindir"
                fi
            fi
            log ln -s "$ci" "$link_target"
            bin_installed=true
        fi
    fi
    if [ -n "$bin_installed" ] && [[ ! ":$PATH:" == *":$bindir:"* ]]; then
        cat >/dev/fd/2 <<EOF
Warning: '$bindir' is not in PATH. You probably have to source ~/.profile again
or launch a new shell.
EOF
    fi
fi

if [ -n "$completion" ]; then
    case "$SHELL" in
    */zsh)
        rc_file="$HOME/.zshrc"
        ;;
    *)
        rc_file="$HOME/.bashrc"
        ;;
    esac

    # Path to register-python-argcomplete is explicited below because
    # by default in Debian-based distributions, ~/.profile first
    # sources ~/.bashrc before adding ~/.local/bin in PATH.

    # We want literal dollar to appear in the string
    # See https://github.com/koalaman/shellcheck/wiki/SC2016#exceptions
    # shellcheck disable=SC2016
    register_command='eval "$($HOME/.local/bin/register-python-argcomplete ci)"'

    if ! grep -e "$register_command" "$rc_file" >&/dev/null; then
        ask sure "Append '$register_command' to '$rc_file'?" y
        if [ "$sure" == y ]; then
            cat >>"$rc_file" <<EOF

# ci.inria.fr command-line completion
$register_command
EOF
        fi
    fi
fi
